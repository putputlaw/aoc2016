# Result

```
Part 1: 299
Part 2: 181
```

# Benchmark

```
part 1                  time:   [6.4171 us 6.4402 us 6.4668 us]
Found 1 outliers among 100 measurements (1.00%)
  1 (1.00%) high mild

part 2                  time:   [130.77 us 131.47 us 132.01 us]
Found 17 outliers among 100 measurements (17.00%)
  9 (9.00%) low severe
  6 (6.00%) high mild
  2 (2.00%) high severe
```

# Comment


