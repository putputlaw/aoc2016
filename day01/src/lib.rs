use common::grid::{ADir, Turn};
use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    insts: Vec<(Turn, i32)>,
}

impl Aoc for Main {
    fn new() -> Self {
        Main { insts: vec![] }
    }

    fn part1(&mut self, input: &str) -> String {
        self.insts = input.split(',').flat_map(parse_inst).collect();
        let origin = (0, 0, ADir::N);
        let (x, y, _) =
            self.insts
                .iter()
                .copied()
                .fold(origin, |(x, y, dir), (hand, num_steps)| {
                    let dir = dir.turn(hand);
                    let (x, y) = match dir {
                        ADir::N => (x, y + num_steps),
                        ADir::E => (x + num_steps, y),
                        ADir::W => (x - num_steps, y),
                        ADir::S => (x, y - num_steps),
                    };
                    (x, y, dir)
                });
        (x.abs() + y.abs()).to_string()
    }

    fn part2(&mut self, _input: &str) -> String {
        let mut visited = std::collections::HashSet::new();
        let mut found = None;
        let origin = (0, 0, ADir::N);
        visited.insert((origin.0, origin.1));
        self.insts
            .iter()
            .copied()
            .fold(origin, |(x, y, dir), (hand, num_steps)| {
                let dir = dir.turn(hand);
                let mut x: i32 = x;
                let mut y: i32 = y;
                for _ in 0..num_steps {
                    match dir {
                        ADir::N => y += 1,
                        ADir::E => x += 1,
                        ADir::W => x -= 1,
                        ADir::S => y -= 1,
                    };
                    if visited.contains(&(x, y)) && found.is_none() {
                        found = Some((x, y));
                    } else {
                        visited.insert((x, y));
                    }
                }
                (x, y, dir)
            });
        found
            .map(|(x, y)| (x.abs() + y.abs()).to_string())
            .unwrap_or("no location visited twice".to_owned())
    }
}

fn parse_inst(input: &str) -> Option<(Turn, i32)> {
    let input = input.trim();
    let num: i32 = input.get(1..)?.parse().ok()?;
    match input.get(0..1) {
        Some("L") => Some((Turn::L, num)),
        Some("R") => Some((Turn::R, num)),
        _ => None,
    }
}
