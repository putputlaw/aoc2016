# Result

```
Part 1: 101
Part 2: 37789
```

# Benchmark

```
part 1                  time:   [445.35 us 446.95 us 448.53 us]
                        change: [+0.1095% +1.0392% +1.8776%] (p = 0.03 < 0.05)
                        Change within noise threshold.
Found 7 outliers among 100 measurements (7.00%)
  3 (3.00%) low mild
  1 (1.00%) high mild
  3 (3.00%) high severe

part 2                  time:   [451.86 us 453.07 us 454.28 us]
                        change: [-42.722% -41.888% -40.987%] (p = 0.00 < 0.05)
                        Performance has improved.
Found 4 outliers among 100 measurements (4.00%)
  1 (1.00%) low mild
  3 (3.00%) high severe
```

# Comment


