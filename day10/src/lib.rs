use common::Aoc;
use std::collections::HashMap;
use std::hash::Hash;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    init_rules: Vec<InitRule>,
    pass_rules: Vec<PassRule>,
}

impl Aoc for Main {
    fn new() -> Self {
        Main {
            init_rules: vec![],
            pass_rules: vec![],
        }
    }

    fn part1(&mut self, input: &str) -> String {
        self.init_rules = input.lines().flat_map(parse_init_rule).collect::<Vec<_>>();
        self.pass_rules = input.lines().flat_map(parse_pass_rule).collect::<Vec<_>>();

        for (bot, (_, low_val), (_, high_val)) in petri(&self.init_rules, &self.pass_rules) {
            if low_val == 17 && high_val == 61 {
                return bot.to_string();
            }
        }
        "no solution".to_string()
    }

    fn part2(&mut self, _input: &str) -> String {
        let mut watch = [None, None, None];

        for (_, (low_ent, low_val), (high_ent, high_val)) in
            petri(&self.init_rules, &self.pass_rules)
        {
            for i in 0..3 {
                if watch[i].is_none() {
                    if let Entity::Output(j) = low_ent {
                        if j == i {
                            watch[i] = Some(low_val);
                        }
                    } else if let Entity::Output(j) = high_ent {
                        if j == i {
                            watch[i] = Some(high_val)
                        }
                    }
                }
            }
            if watch.iter().all(|w| w.is_some()) {
                return watch
                    .iter()
                    .flatten()
                    .copied()
                    .product::<usize>()
                    .to_string();
            }
        }
        "no solution".to_string()
    }
}

#[derive(Clone, Copy, Debug)]
struct InitRule {
    value: usize,
    bot: usize,
}
#[derive(Clone, Copy, Debug)]
struct PassRule {
    bot: usize,
    low: Entity,
    high: Entity,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
enum Entity {
    Output(usize),
    Bot(usize),
}

impl Entity {
    fn parse(s: &str) -> Option<Entity> {
        match s.split_ascii_whitespace().collect::<Vec<_>>()[..] {
            ["bot", id] => Some(Entity::Bot(id.parse().ok()?)),
            ["output", id] => Some(Entity::Output(id.parse().ok()?)),
            _ => None,
        }
    }

    fn is_output(self) -> bool {
        match self {
            Entity::Output(_) => true,
            _ => false,
        }
    }
}

// value 23 goes to bot 8
fn parse_init_rule(s: &str) -> Option<InitRule> {
    match s.split_ascii_whitespace().collect::<Vec<_>>()[..] {
        ["value", value, "goes", "to", "bot", bot] => Some(InitRule {
            value: value.parse().ok()?,
            bot: bot.parse().ok()?,
        }),
        _ => None,
    }
}

// bot 32 gives low to output 12 and high to bot 6
fn parse_pass_rule(s: &str) -> Option<PassRule> {
    match s.split_ascii_whitespace().collect::<Vec<_>>()[..] {
        ["bot", bot, "gives", "low", "to", role_low, id_low, "and", "high", "to", role_high, id_high] => {
            Some(PassRule {
                bot: bot.parse().ok()?,
                low: Entity::parse(&[role_low, id_low].join(" "))?,
                high: Entity::parse(&[role_high, id_high].join(" "))?,
            })
        }
        _ => None,
    }
}

fn petri<'a>(
    init_rules: &'a [InitRule],
    pass_rules: &'a [PassRule],
) -> impl Iterator<Item = (usize, (Entity, usize), (Entity, usize))> + 'a {
    let mut entities: HashMap<Entity, Vec<usize>> = HashMap::new();
    for InitRule { value, bot } in init_rules.iter().copied() {
        entities.entry(Entity::Bot(bot)).or_default().push(value)
    }
    let mut i = 0;

    std::iter::from_fn(move || loop {
        let PassRule { bot, low, high } = pass_rules[i];

        let chips_len = entities.entry(Entity::Bot(bot)).or_default().len();
        let low_chips_len = entities.entry(low).or_default().len();
        let high_chips_len = entities.entry(high).or_default().len();
        if chips_len == 2
            && (low.is_output() || low_chips_len < 2)
            && (high.is_output() || high_chips_len < 2)
        {
            let chips = entities.entry(Entity::Bot(bot)).or_default();
            chips.sort();
            let high_val = chips.pop().unwrap();
            let low_val = chips.pop().unwrap();

            let low_chips = entities.entry(low).or_default();
            low_chips.push(low_val);
            let high_chips = entities.entry(high).or_default();
            high_chips.push(high_val);

            i += 1;
            i %= pass_rules.len();

            return Some((bot, (low, low_val), (high, high_val)));
        } else {
            i += 1;
            i %= pass_rules.len();
        }
    })
}
