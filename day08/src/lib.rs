use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

const WIDTH: usize = 50;
const HEIGHT: usize = 6;

pub struct Main {
    disp: Vec<Vec<bool>>,
}

impl Aoc for Main {
    fn new() -> Self {
        Main { disp: vec![] }
    }

    fn part1(&mut self, input: &str) -> String {
        self.disp = vec![vec![false; HEIGHT]; WIDTH];
        for inst in input.lines().flat_map(Inst::parse) {
            match inst {
                Inst::Rect(w, h) => {
                    for x in 0..w {
                        for y in 0..h {
                            self.disp[x][y] = true;
                        }
                    }
                }
                Inst::Row(y, n) => {
                    let row = (0..WIDTH).map(|x| self.disp[x][y]).collect::<Vec<_>>();
                    for x in 0..WIDTH {
                        self.disp[(x + n) % WIDTH][y] = row[x]
                    }
                }
                Inst::Col(x, n) => {
                    let col = (0..HEIGHT).map(|y| self.disp[x][y]).collect::<Vec<_>>();
                    for y in 0..HEIGHT {
                        self.disp[x][(y + n) % HEIGHT] = col[y]
                    }
                }
            }
        }

        self.disp
            .iter()
            .flatten()
            .filter(|x| **x)
            .count()
            .to_string()
    }

    fn part2(&mut self, _input: &str) -> String {
        format!(
            "\n{}",
            (0..HEIGHT)
                .map(|y| {
                    (0..WIDTH)
                        .map(|x| if self.disp[x][y] { '\u{2588}' } else { '.' })
                        .collect::<String>()
                })
                .collect::<Vec<_>>()
                .join("\n")
        )
    }
}

enum Inst {
    Rect(usize, usize),
    Row(usize, usize),
    Col(usize, usize),
}

impl Inst {
    fn parse(s: &str) -> Option<Self> {
        let words = s.split_whitespace().collect::<Vec<_>>();
        match &words[..] {
            ["rect", a_times_b] => {
                if let [a, b] = &a_times_b.split("x").collect::<Vec<_>>()[..] {
                    Some(Inst::Rect(a.parse().ok()?, b.parse().ok()?))
                } else {
                    None
                }
            }
            ["rotate", "row", a, "by", b] => {
                Some(Inst::Row(a.get(2..)?.parse().ok()?, b.parse().ok()?))
            }
            ["rotate", "column", a, "by", b] => {
                Some(Inst::Col(a.get(2..)?.parse().ok()?, b.parse().ok()?))
            }
            _ => {
                println!("{:?}", &words);
                None
            }
        }
    }
}
