fn main() {
    println!("----- INPUT -----");
    println!("{}", day08::INPUT);
    println!("-----------------");

    use common::Aoc;
    day08::Main::new().run(day08::INPUT);
}
