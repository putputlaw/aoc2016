# Result

```
Part 1: 245102
Part 2: 324
```

# Benchmark

```
part 1                  time:   [2.6177 ms 2.6223 ms 2.6259 ms]
Found 15 outliers among 100 measurements (15.00%)
  2 (2.00%) low severe
  4 (4.00%) low mild
  3 (3.00%) high mild
  6 (6.00%) high severe

part 2                  time:   [68.985 us 69.328 us 69.708 us]
Found 5 outliers among 100 measurements (5.00%)
  5 (5.00%) high mild
```

# Comment


