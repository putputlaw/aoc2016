fn main() {
    println!("----- INPUT -----");
    println!("{}", day04::INPUT);
    println!("-----------------");

    use common::Aoc;
    day04::Main::new().run(day04::INPUT);
}
