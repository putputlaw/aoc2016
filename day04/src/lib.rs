use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    names: Vec<Name>,
}

impl Aoc for Main {
    fn new() -> Self {
        Main { names: vec![] }
    }

    fn part1(&mut self, input: &str) -> String {
        self.names = input
            .lines()
            .flat_map(parse)
            .filter(|name| name.checksum == checksum(&name.enc_name))
            .collect();
        self.names
            .iter()
            .map(|name| name.sector_id)
            .sum::<u32>()
            .to_string()
    }

    fn part2(&mut self, _input: &str) -> String {
        for name in self.names.iter() {
            let dec_name: Vec<_> = name
                .enc_name
                .iter()
                .map(|p| rotate(&*p, name.sector_id))
                .collect();
            let dec_name = dec_name.join("-");
            if &dec_name == "northpole-object-storage" {
                return name.sector_id.to_string();
            }
        }
        "not found".to_string()
    }
}

// Each room consists of an encrypted name (lowercase letters separated by dashes) followed by a dash, a sector ID, and a checksum in square brackets.

#[derive(Clone, Debug)]
struct Name {
    enc_name: Vec<String>,
    sector_id: u32,
    checksum: String,
}

fn parse(name: &str) -> Option<Name> {
    let pieces: Vec<&str> = name
        .split(|c| c == '-' || c == '[' || c == ']')
        .filter(|s| !s.is_empty())
        .collect();
    let n = pieces.len();
    if n >= 3 {
        Some(Name {
            enc_name: pieces[0..n - 2].iter().map(|s| s.to_string()).collect(),
            sector_id: pieces[n - 2].parse().ok()?,
            checksum: pieces[n - 1].to_owned(),
        })
    } else {
        None
    }
}

fn checksum<S: AsRef<str>>(pieces: &[S]) -> String {
    let mut counter = std::collections::HashMap::<char, i32>::new();
    for p in pieces {
        p.as_ref()
            .chars()
            .fold((), |(), c| *counter.entry(c).or_default() -= 1)
    }
    let mut checksum: Vec<_> = counter.iter().map(|(c, n)| (n, c)).collect();
    checksum.sort();
    checksum.into_iter().map(|(_, c)| *c).take(5).collect()
}

fn rotate(s: &str, num: u32) -> String {
    s.chars()
        .map(|c| (((c as u32 - 'a' as u32) + num) % 26 + 'a' as u32) as u8 as char)
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn validation() {
        // aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
        // a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
        // not-a-real-room-404[oarel] is a real room.
        // totally-real-room-200[decoy] is not.

        assert_eq!(
            "abxyz".to_string(),
            checksum(&["aaaaa", "bbb", "z", "y", "x"])
        );

        assert_eq!(
            "abcde".to_string(),
            checksum(&["a", "b", "c", "d", "e", "f", "g", "h"])
        );

        assert_eq!("oarel".to_string(), checksum(&["not", "a", "real", "room"]));

        assert!("decoy".to_string() != checksum(&["totally", "real", "room"]));
    }
}
