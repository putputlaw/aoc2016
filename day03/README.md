# Result

```
Part 1: 917
Part 2: 1649
```

# Benchmark

```
part 1                  time:   [334.70 us 338.73 us 341.78 us]
Found 19 outliers among 100 measurements (19.00%)
  7 (7.00%) low severe
  2 (2.00%) low mild
  5 (5.00%) high mild
  5 (5.00%) high severe

part 2                  time:   [7.1435 us 7.1954 us 7.2524 us]
Found 8 outliers among 100 measurements (8.00%)
  6 (6.00%) high mild
  2 (2.00%) high severe
```

# Comment


