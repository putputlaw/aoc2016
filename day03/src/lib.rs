use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    data: Vec<V3>,
}

impl Aoc for Main {
    fn new() -> Self {
        Main { data: vec![] }
    }

    fn part1(&mut self, input: &str) -> String {
        self.data = input
            .lines()
            .flat_map(|l| v3(common::text::words(l)))
            .collect();
        self.data
            .iter()
            .filter(|v| possible(**v))
            .count()
            .to_string()
    }

    fn part2(&mut self, _input: &str) -> String {
        let mut it = self.data.iter().copied();
        let mut triples = vec![];
        while let Some((v1, v2, v3)) = transpose(&mut it) {
            triples.push(v1);
            triples.push(v2);
            triples.push(v3);
        }
        triples.iter().filter(|v| possible(**v)).count().to_string()
    }
}

type V3 = (u32, u32, u32);

fn v3<'a, I: Iterator<Item = &'a str>>(mut it: I) -> Option<V3> {
    let x = it.next()?.trim().parse().ok()?;
    let y = it.next()?.trim().parse().ok()?;
    let z = it.next()?.trim().parse().ok()?;
    Some((x, y, z))
}

fn possible(v: V3) -> bool {
    v.0 + v.1 > v.2 && v.0 + v.2 > v.1 && v.1 + v.2 > v.0
}

fn transpose<I>(it: &mut I) -> Option<(V3, V3, V3)>
where
    I: Iterator<Item = V3>,
{
    let (x0, y0, z0) = it.next()?;
    let (x1, y1, z1) = it.next()?;
    let (x2, y2, z2) = it.next()?;
    Some(((x0, x1, x2), (y0, y1, y2), (z0, z1, z2)))
}
