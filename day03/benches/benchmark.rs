use criterion::{black_box, criterion_group, criterion_main, Criterion};

fn criterion_benchmark(c: &mut Criterion) {
    use common::Aoc;
    let mut main = day03::Main::new();
    c.bench_function("part 1", |b| b.iter(|| main.part1(black_box(day03::INPUT))));
    c.bench_function("part 2", |b| b.iter(|| main.part2(black_box(day03::INPUT))));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);

