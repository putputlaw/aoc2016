use common::grid::ADir;
use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    inputs: Vec<Vec<ADir>>,
}

impl Aoc for Main {
    fn new() -> Self {
        Main { inputs: vec![] }
    }

    fn part1(&mut self, input: &str) -> String {
        self.inputs = input
            .lines()
            .map(|l| l.chars().flat_map(parse_dir).collect())
            .collect();

        let numpad = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
        let mut row = 1;
        let mut col = 1;

        let nums: Vec<_> = self
            .inputs
            .iter()
            .map(|l| {
                for d in l {
                    match d {
                        ADir::N => {
                            if row > 0 {
                                row -= 1
                            }
                        }
                        ADir::W => {
                            if col > 0 {
                                col -= 1
                            }
                        }
                        ADir::S => {
                            if row + 1 < 3 {
                                row += 1
                            }
                        }
                        ADir::E => {
                            if col + 1 < 3 {
                                col += 1
                            }
                        }
                    }
                }
                numpad[row][col]
            })
            .collect();
        nums.iter().map(|n| n.to_string()).collect()
    }

    fn part2(&mut self, _input: &str) -> String {
        let numpad = common::grid::parse(
            "
--1--
-234-
56789
-ABC-
--D--
"
            .trim(),
        )
        .0;

        let ok = |row, col| {
            0 <= row && row < 5 && 0 <= col && col < 5 && numpad[row as usize][col as usize] != '-'
        };

        let mut row = 2i32;
        let mut col = 0i32;

        let nums: Vec<_> = self
            .inputs
            .iter()
            .map(|l| {
                for d in l {
                    match d {
                        ADir::N => {
                            if ok(row - 1, col) {
                                row -= 1
                            }
                        }
                        ADir::W => {
                            if ok(row, col - 1) {
                                col -= 1
                            }
                        }
                        ADir::S => {
                            if ok(row + 1, col) {
                                row += 1
                            }
                        }
                        ADir::E => {
                            if ok(row, col + 1) {
                                col += 1
                            }
                        }
                    }
                }
                numpad[row as usize][col as usize]
            })
            .collect();
        nums.iter().map(|n| n.to_string()).collect()
    }
}

fn parse_dir(c: char) -> Option<ADir> {
    match c {
        'U' => Some(ADir::N),
        'L' => Some(ADir::W),
        'D' => Some(ADir::S),
        'R' => Some(ADir::E),
        _ => None,
    }
}
