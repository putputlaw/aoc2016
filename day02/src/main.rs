
fn main() {
    println!("----- INPUT -----");
    println!("{}", day02::INPUT);
    println!("-----------------");

    use common::Aoc;
    day02::Main::new().run(day02::INPUT);
}
