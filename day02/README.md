# Result

```
Part 1: 98575
Part 2: CD8D4
```

# Benchmark

```
part 1                  time:   [60.859 us 61.548 us 62.106 us]
Found 28 outliers among 100 measurements (28.00%)
  20 (20.00%) low severe
  5 (5.00%) high mild
  3 (3.00%) high severe

part 2                  time:   [28.512 us 28.553 us 28.585 us]
Found 12 outliers among 100 measurements (12.00%)
  2 (2.00%) low severe
  2 (2.00%) high mild
  8 (8.00%) high severe
```

# Comment


