use common::Aoc;
use std::collections::HashSet;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {}

impl Aoc for Main {
    fn new() -> Self {
        Main {}
    }

    fn part1(&mut self, input: &str) -> String {
        input.lines().filter(|l| is_tls(l)).count().to_string()
    }

    fn part2(&mut self, input: &str) -> String {
        input.lines().filter(|l| is_ssl(l)).count().to_string()
    }
}

fn has_abba(s: &str) -> bool {
    if s.len() >= 4 {
        for i in 0..s.len() - 3 {
            let a = s.get(i + 0..i + 1);
            let b = s.get(i + 1..i + 2);
            let c = s.get(i + 2..i + 3);
            let d = s.get(i + 3..i + 4);
            if a == d && b == c && a != b {
                return true;
            }
        }
        false
    } else {
        false
    }
}

fn is_tls(s: &str) -> bool {
    #[derive(Copy, Clone)]
    enum State {
        NotTLS,
        MaybeNotTLS,
        MaybeTLS,
    }

    match s
        .split(|c| "[]".contains(c))
        .map(has_abba)
        .enumerate()
        .fold(State::MaybeNotTLS, |acc, (i, has_abba)| {
            match (i % 2 == 0, has_abba, acc) {
                (_, _, State::NotTLS) => State::NotTLS,
                (true, true, State::MaybeNotTLS) => State::MaybeTLS,
                (false, true, _) => State::NotTLS,
                _ => acc,
            }
        }) {
        State::MaybeTLS => true,
        _ => false,
    }
}

fn set_of_abas(s: &str) -> HashSet<(char, char)> {
    let chars = s.chars().collect::<Vec<_>>();
    if chars.len() >= 3 {
        (0..chars.len() - 2)
            .flat_map(|i| {
                let a = *chars.get(i + 0)?;
                let b = *chars.get(i + 1)?;
                let c = *chars.get(i + 2)?;
                Some((a, b)).filter(|_| a == c && a != b)
            })
            .collect()
    } else {
        HashSet::new()
    }
}

fn is_ssl(s: &str) -> bool {
    let mut inside = HashSet::new();
    let mut outside = HashSet::new();
    for (i, w) in s.split(|c| "[]".contains(c)).enumerate() {
        if i % 2 == 0 {
            inside.extend(set_of_abas(w).iter().copied())
        } else {
            outside.extend(set_of_abas(w).iter().map(|(b, a)| (*a, *b)))
        }
    }
    inside.intersection(&outside).nth(0).is_some()
}
