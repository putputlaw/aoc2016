# Result

```
Part 1: 110
Part 2: 242
```

# Benchmark

```
part 1                  time:   [2.0111 ms 2.0194 ms 2.0277 ms]
Found 3 outliers among 100 measurements (3.00%)
  3 (3.00%) high mild

part 2                  time:   [4.3810 ms 4.4162 ms 4.4456 ms]
Found 12 outliers among 100 measurements (12.00%)
  6 (6.00%) low severe
  1 (1.00%) low mild
  4 (4.00%) high mild
  1 (1.00%) high severe
```

# Comment


