# Result

```
Part 1: 150914
Part 2: 11052855125
```

# Benchmark

```
part 1                  time:   [17.157 us 17.221 us 17.272 us]
Found 10 outliers among 100 measurements (10.00%)
  6 (6.00%) low severe
  1 (1.00%) low mild
  3 (3.00%) high mild

part 2                  time:   [146.57 us 147.77 us 148.70 us]
Found 13 outliers among 100 measurements (13.00%)
  10 (10.00%) low severe
  1 (1.00%) low mild
  1 (1.00%) high mild
  1 (1.00%) high severe
```

# Comment

Sweet recursion.
