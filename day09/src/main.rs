fn main() {
    println!("----- INPUT -----");
    println!("{}", day09::INPUT);
    println!("-----------------");

    use common::Aoc;
    day09::Main::new().run(day09::INPUT);
}
