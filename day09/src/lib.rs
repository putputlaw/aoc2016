use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    chars: Vec<char>,
}

impl Aoc for Main {
    fn new() -> Self {
        Main {chars:vec![]}
    }

    fn part1(&mut self, input: &str) -> String {
        self.chars = input.trim().chars().collect::<Vec<_>>();
        let mut cref = &self.chars[..];
        let mut count = 0;
        while let Some(step) = next(cref) {
            match step {
                Step::Char(_, next_ref) => {
                    count += 1;
                    cref = next_ref;
                }
                Step::Expand(times, repeat, next_ref) => {
                    count += times * repeat.len();
                    cref = next_ref;
                }
            }
        }
        count.to_string()
    }

    fn part2(&mut self, _input: &str) -> String {
        solve(&self.chars[..]).to_string()
    }
}

enum Step<'a> {
    Char(char, &'a [char]),
    Expand(usize, &'a [char], &'a [char]),
}

fn next(s: &[char]) -> Option<Step<'_>> {
    if let Some(&c) = s.get(0) {
        if c == '(' {
            let mut j = 0;
            while s.get(j) != Some(&'x') {
                j += 1;
            }
            let mut k = j;
            while s.get(k) != Some(&')') {
                k += 1;
            }
            let a = s[1..j]
                .iter()
                .copied()
                .collect::<String>()
                .parse::<usize>()
                .ok()?;
            let b = s[j + 1..k]
                .iter()
                .copied()
                .collect::<String>()
                .parse::<usize>()
                .ok()?;
            Some(Step::Expand(b, &s[k + 1..k + a + 1], &s[k + a + 1..]))
        } else {
            Some(Step::Char(c, &s[1..]))
        }
    } else {
        None
    }
}

fn solve(s: &[char]) -> usize {
    match next(s) {
        None => 0,
        Some(Step::Char(_, rest)) => 1 + solve(rest),
        Some(Step::Expand(times, repeat, rest)) => times * solve(repeat) + solve(rest),
    }
}
