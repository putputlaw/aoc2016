#[derive(Debug, Clone, Copy)]
pub struct Size {
    pub width: usize,
    pub height: usize,
}

impl Size {
    pub fn new(width: usize, height: usize) -> Self {
        Size { width, height }
    }
}

/// returns grid data, size, and hint whether all rows have uniform width
pub fn parse(input: &str) -> (Vec<Vec<char>>, Size, bool) {
    let grid: Vec<Vec<_>> = input.lines().map(|line| line.chars().collect()).collect();
    let size = Size {
        height: grid.len(),
        width: grid[0].len(),
    };
    let uniform = grid.iter().all(|line| line.len() == size.width);
    (grid, size, uniform)
}

/// returns grid data, size, and hint whether all rows have uniform width
pub fn parse_with<F, T>(input: &str, f: F) -> Option<(Vec<Vec<T>>, Size, bool)>
where
    F: Fn(char) -> Option<T>,
{
    let (grid, size, uniform) = parse(input);
    let grid: Option<Vec<Vec<T>>> = grid
        .iter()
        .map(|line| line.iter().map(|c| f(*c)).collect::<Option<Vec<_>>>())
        .collect();
    Some((grid?, size, uniform))
}

pub const ALL: [bool; 8] = [true, true, true, true, true, true, true, true];

pub const NESW: [bool; 8] = [false, true, false, true, true, false, true, false];

/// grid neighbourhood
///
/// matrix example for {north, east, south, west}:
///     [
///         false, true,  false,
///         true,         true,
///         false, true,  false,
///     ]
pub fn neigh(matrix: [bool; 8], size: Size, (i, j): (usize, usize)) -> Vec<(usize, usize)> {
    assert!(i < size.height);
    assert!(j < size.width);
    let [nw, n, ne, w, e, sw, s, se] = matrix;
    let mut neighs = vec![];
    if nw && i > 0 && j > 0 {
        neighs.push((i - 1, j - 1))
    }
    if n && i > 0 {
        neighs.push((i - 1, j))
    }
    if ne && i > 0 && j + 1 < size.width {
        neighs.push((i - 1, j + 1))
    }
    if w && j > 0 {
        neighs.push((i, j - 1))
    }
    if e && j + 1 < size.width {
        neighs.push((i, j + 1))
    }
    if sw && i + 1 < size.height && j > 0 {
        neighs.push((i + 1, j - 1))
    }
    if s && i + 1 < size.height {
        neighs.push((i + 1, j))
    }
    if se && i + 1 < size.height && j + 1 < size.width {
        neighs.push((i + 1, j + 1))
    }
    neighs
}

/// absolute direction (north, east, south, west)
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ADir {
    N,
    E,
    S,
    W,
}

impl ADir {
    pub fn turn(self, turn: Turn) -> Self {
        match turn {
            Turn::L => match self {
                ADir::N => ADir::W,
                ADir::E => ADir::N,
                ADir::S => ADir::E,
                ADir::W => ADir::S,
            },

            Turn::R => match self {
                ADir::N => ADir::E,
                ADir::E => ADir::S,
                ADir::S => ADir::W,
                ADir::W => ADir::N,
            },
        }
    }

    pub fn flip(self) -> Self {
        match self {
            ADir::N => ADir::S,
            ADir::E => ADir::W,
            ADir::S => ADir::N,
            ADir::W => ADir::E,
        }
    }

    pub fn mv_xy(self, (x, y): (i32, i32), steps: i32) -> (i32, i32) {
        match self {
            ADir::N => (x, y + steps),
            ADir::E => (x + steps, y),
            ADir::S => (x, y - steps),
            ADir::W => (x - steps, y),
        }
    }

    pub fn mv_rc(self, (row, col): (usize, usize), steps: usize) -> Option<(usize, usize)> {
        match self {
            ADir::N => {
                if row >= steps {
                    Some((row - steps, col))
                } else {
                    None
                }
            }
            ADir::E => Some((row, col + steps)),
            ADir::S => Some((row + steps, col)),
            ADir::W => {
                if col >= steps {
                    Some((row, col - steps))
                } else {
                    None
                }
            }
        }
    }
}

/// relative direction (forward, backward, left, right)
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RDir {
    F,
    B,
    L,
    R,
}

impl RDir {
    /// absolute direction from relative direction + facing direction
    pub fn abs(self, facing: ADir) -> ADir {
        match self {
            RDir::F => facing,
            RDir::B => facing.turn(Turn::L).turn(Turn::L),
            RDir::L => facing.turn(Turn::L),
            RDir::R => facing.turn(Turn::R),
        }
    }
}

impl From<Turn> for RDir {
    fn from(turn: Turn) -> Self {
        match turn {
            Turn::L => RDir::L,
            Turn::R => RDir::R,
        }
    }
}

/// turn (left or right) by 90 degrees
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Turn {
    L,
    R,
}
