pub use priority_queue as pq;

/// Shortest paths and traversal
/// ===
pub mod paths;


/// Grids
/// =====
/// ```
/// [
///     (0,0),   (0,1),   (0,2),   (0,3),
///     (1,0),   (1,1),   (1,2),   (1,3),
///     (2,0),   (2,1),   (2,2),   (2,3),
///     (3,0),   (3,1),   (3,2),   (3,3),
/// ];
/// ```
pub mod grid;


/// Text processing
/// ===
pub mod text;

/// Aoc trait
pub trait Aoc {
    fn new() -> Self;
    fn part1(&mut self, input: &str) -> String;
    fn part2(&mut self, _input: &str) -> String;
    fn run(&mut self, input: &str)
    where
        Self: std::marker::Sized,
    {
        println!("Part 1: {}", self.part1(input));
        println!("Part 2: {}", self.part2(input));
    }
}
