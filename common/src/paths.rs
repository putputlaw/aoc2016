use crate::pq;
use std::cmp::Reverse;
use std::hash::Hash;

/// dijkstra
pub fn dijkstra<T, FN>(start: T, neigh: FN) -> impl Iterator<Item = (T, u64)>
where
    T: Clone + Hash + Eq,
    FN: Fn(&T) -> Vec<(T, u64)>,
{
    let mut distances: std::collections::HashMap<T, u64> = std::collections::HashMap::new();
    distances.insert(start.clone(), 0);
    let mut q = pq::PriorityQueue::new();
    q.push(start, Reverse(0u64));
    std::iter::from_fn(move || {
        let (node, Reverse(dist)) = q.pop()?;
        for (tonari, delta) in neigh(&node) {
            if let Some(ndist) = distances.get_mut(&tonari) {
                *ndist = (*ndist).min(dist + delta);
            } else {
                distances.insert(tonari.clone(), dist + delta);
                q.push_increase(tonari, Reverse(dist + delta));
            }
        }
        Some((node, dist.to_owned()))
    })
}

/// breadth first search
pub fn bfs<'a, T, FN>(start: T, neigh: FN) -> impl Iterator<Item = T>
where
    T: Hash + Eq + Clone,
    FN: Fn(&T) -> Vec<T>,
{
    let mut visited = std::collections::HashSet::new();
    let mut q = std::collections::VecDeque::new();
    q.push_back(start);
    std::iter::from_fn(move || {
        while visited.contains(q.get(0)?) {
            q.pop_front();
        }
        let node = q.pop_front()?;
        visited.insert(node.clone());
        for tonari in neigh(&node) {
            if !visited.contains(&tonari) {
                q.push_back(tonari)
            }
        }
        Some(node)
    })
}

/// depth first search
pub fn dfs<'a, T, FN>(start: T, neigh: FN) -> impl Iterator<Item = T>
where
    T: Hash + Eq + Clone,
    FN: Fn(&T) -> Vec<T>,
{
    let mut visited = std::collections::HashSet::new();
    let mut q = std::collections::VecDeque::new();
    q.push_back(start);
    std::iter::from_fn(move || {
        while visited.contains(q.get(0)?) {
            q.pop_front();
        }
        let node = q.pop_front()?;
        visited.insert(node.clone());
        for tonari in neigh(&node) {
            if !visited.contains(&tonari) {
                q.push_front(tonari)
            }
        }
        Some(node)
    })
}
