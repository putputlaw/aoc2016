fn main() {
    let size = common::grid::Size::new(5, 5);
    let u: Vec<(_, _)> = common::paths::dijkstra((0, 0), |idx| {
        common::grid::neigh(common::grid::NESW, size, *idx)
            .into_iter()
            .map(|x| (x, 1))
            .collect()
    })
    .collect();
    println!("{:?}", u);
}
