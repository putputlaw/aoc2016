# Result

```
Part 1: ikerpcty
Part 2: uwpfaqrq
```

# Benchmark

```
part 1                  time:   [1.0027 ms 1.0180 ms 1.0300 ms]
Found 20 outliers among 100 measurements (20.00%)
  12 (12.00%) low severe
  3 (3.00%) low mild
  3 (3.00%) high mild
  2 (2.00%) high severe

part 2                  time:   [561.88 ns 565.13 ns 568.39 ns]
Found 2 outliers among 100 measurements (2.00%)
  1 (1.00%) low mild
  1 (1.00%) high mild
```

# Comment


