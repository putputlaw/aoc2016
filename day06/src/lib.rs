use common::Aoc;
use std::collections::HashMap;
use std::hash::Hash;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
    dist: Vec<MultiSet<char>>,
}

impl Aoc for Main {
    fn new() -> Self {
        Main { dist: vec![] }
    }

    fn part1(&mut self, input: &str) -> String {
        self.dist = input
            .lines()
            .map(|l| l.chars().map(singleton).collect::<Vec<_>>())
            .fold(vec![MultiSet::new(); 8], |acc, mset_vec| {
                (0..acc.len())
                    .map(|i| union(acc[i].clone(), &mset_vec[i]))
                    .collect()
            });

        self.dist
            .iter()
            .flat_map(|mset| mset.into_iter().max_by_key(|(_, v)| *v).map(|(k, _)| k))
            .collect()
    }

    fn part2(&mut self, _input: &str) -> String {
        self.dist
            .iter()
            .flat_map(|mset| mset.into_iter().min_by_key(|(_, v)| *v).map(|(k, _)| k))
            .collect()
    }
}

type MultiSet<T> = HashMap<T, usize>;
fn singleton<T>(x: T) -> MultiSet<T>
where
    T: Eq + Hash,
{
    vec![(x, 1)].into_iter().collect()
}

fn union<T>(mut a: MultiSet<T>, b: &MultiSet<T>) -> MultiSet<T>
where
    T: Hash + Eq + Clone,
{
    for (k, num) in b {
        *a.entry(k.clone()).or_default() += num
    }
    a
}
