
fn main() {
    println!("----- INPUT -----");
    println!("{}", day05::INPUT);
    println!("-----------------");

    use common::Aoc;
    day05::Main::new().run(day05::INPUT);
}
