use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {}

impl Aoc for Main {
    fn new() -> Self {
        Main {}
    }

    fn part1(&mut self, input: &str) -> String {
        let input = input.trim();
        (0..)
            .filter_map(|i| {
                let cat = [input, i.to_string().as_ref()].concat();
                let dig = md5::compute(cat.as_bytes());
                let fmt = format!("{:x}", dig);
                if fmt.get(0..5) == Some("00000") {
                    fmt.get(5..6).map(|s| s.to_owned())
                } else {
                    None
                }
            })
            .take(8)
            .collect()
    }

    fn part2(&mut self, input: &str) -> String {
        let input = input.trim();
        let mut fertig = vec![None; 8];
        (0..)
            .filter_map(|i| {
                let cat = [input, i.to_string().as_ref()].concat();
                let dig = md5::compute(cat.as_bytes());
                let fmt = format!("{:x}", dig);
                if fmt.get(0..5) == Some("00000") {
                    if let Some(i) = fmt.get(5..6).and_then(|i| i.parse::<usize>().ok()) {
                        if i < 8 && fertig[i].is_none() {
                            fertig[i] = fmt.chars().nth(6);
                            return Some(())
                        }
                    }
                }
                None
            })
            .take(8)
            .for_each(|_| ());
        fertig.iter().flatten().collect()
    }
}
