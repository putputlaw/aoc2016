#!/usr/bin/env fish

set num (ls | rg -ie "^day" | wc -l)
set num (math $num + 1)
if [ $num -lt 10 ]
    set num "0"$num
end
set repo "day"$num


cargo new --vcs none $repo


mkdir $repo/benches
touch $repo/input.txt
echo 'common = { path = "../common" }
#itertools = "0.9"
#regex = "1.4.2"

[dev-dependencies]
criterion = "0.3.3"

[[bench]]
name = "benchmark"
harness = false' >> $repo/Cargo.toml
echo "fn main() {
    println!(\"----- INPUT -----\");
    println!(\"{}\", $repo::INPUT);
    println!(\"-----------------\");

    use common::Aoc;
    $repo::Main::new().run($repo::INPUT);
}" > $repo/src/main.rs
echo 'use common::Aoc;

pub const INPUT: &str = include_str!("../input.txt");

pub struct Main {
}

impl Aoc for Main {
    fn new() -> Self {
        Main {
        }
    }

    fn part1(&mut self, input: &str) -> String {
        "not yet".to_owned()
    }

    fn part2(&mut self, _input: &str) -> String {
        "not yet".to_owned()
    }
}'> $repo/src/lib.rs
echo '# Result

```

```

# Benchmark

```

```

# Comment

' > $repo/README.md
echo "use criterion::{black_box, criterion_group, criterion_main, Criterion};

fn criterion_benchmark(c: &mut Criterion) {
    use common::Aoc;
    let mut main = $repo::Main::new();
    c.bench_function(\"part 1\", |b| b.iter(|| main.part1(black_box($repo::INPUT))));
    c.bench_function(\"part 2\", |b| b.iter(|| main.part2(black_box($repo::INPUT))));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
" > $repo/benches/benchmark.rs
